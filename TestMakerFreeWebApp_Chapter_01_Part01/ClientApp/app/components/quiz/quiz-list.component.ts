﻿import { Component, Inject, Input, OnInit } from "@angular/core";
import { HttpClient } from "@angular/common/http";
@Component({
	selector: "quiz-list",
	templateUrl: './quiz-list.component.html',
	styleUrls: ['./quiz-list.component.css']
})
export class QuizListComponent {
	title: string;
	selectedQuiz: Quiz;
	quizzes: Quiz[];
	http: HttpClient;
	
	constructor(http: HttpClient,
		@Inject('BASE_URL') baseUrl: string) {
		this.title = "Latest Quizzes";
		this.http = http;
		var url = baseUrl + "api/quiz/Latest/10";
		this.http.get<Quiz[]>(url).subscribe(result => {
			this.quizzes = result;
		}, error => console.error(error));
	}
	onSelect(quiz: Quiz) {
		this.selectedQuiz = quiz;
		console.log("quiz with Id "
			+ this.selectedQuiz.Id
			+ " has been selected.");
	}
}